package com.ljqc.sbom.management.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pagination<T> {

    private long total = 0;
    @Builder.Default
    private Collection<T> records = new ArrayList<>();

}
