package com.ljqc.sbom.management.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * request header key
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RequestHeaderKey {
    // userId
    public static final String X_LJQC_UID = "X-LJQC-Uid";
}
