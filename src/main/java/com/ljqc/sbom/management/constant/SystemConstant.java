package com.ljqc.sbom.management.constant;

/**
 * @Author lch
 * @Date 2022/5/5 8:33
 **/
public class SystemConstant {
    public static String STR_0 = "0";

    public static String STR_1 = "1";
    public static String STR_3 = "3";
    public static String STR_4 = "4";
    public static Integer INT_0 = 0;
    public static Integer INT_1 = 1;
    public static Integer INT_2 = 2;
    public static Integer INT_3 = 3;
    public static Integer INT_4 = 4;

    public static String TARGET = "target";
    public static String COUNT = "count";
    public static String LINE = "line";
    public static String MATCH_LINE = "match_line";
    public static String FILE_TYPE = "file_type";
    public static String MATCH_TYPE = "match_type";


    public static String TYPE = "type";
    public static String LEVEL = "level";
    public static String DIRECT_COUNT = "directCount";
    public static String INDIRECT_COUNT = "indirectCount";
    public static String BINARY_COUNT = "binaryCount";
    public static String TOTAL = "total";
    public static String SERIOUS = "serious";
    public static String HIGH = "high";
    public static String MID = "mid";
    public static String LOW = "low";
    public static String NOT_RATED = "notRated";
    public static String NO_RISK = "noRisk";
    public static String UN_DECLARED = "undeclared";

    /**
     *  组件
     */
    public static final Integer COMPONENT = 0;

    /**
     *  源码
     */
    public static final Integer SOURCE_CODE = 1;

    /**
     * 项目
     */
    public static final Integer PROJECT_CODE = 2;

    /**
     *  许可证条件兼容
     */
    public static final Integer COMPATIBLE = 1;

    /**
     *  许可证不兼容
     */
    public static final Integer NOT_COMPATIBLE = 2;

    /**
     *  忽略
     */
    public static final Integer IGNORE = 1;

    /**
     *  不忽略
     */
    public static final Integer NOT_IGNORE = 0;
}
