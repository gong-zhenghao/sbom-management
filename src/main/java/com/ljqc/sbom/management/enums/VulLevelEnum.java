package com.ljqc.sbom.management.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * VUL_LEVEL枚举
 */
@Getter
@AllArgsConstructor
public enum VulLevelEnum {

    UNKNOWN(0, "未知", "unknown"),
    LOW(1, "低危", "low"),
    MID(2, "中危", "mid"),
    HIGH(3, "高危", "high"),
    CRITICAL(4, "严重", "critical");

    private final Integer code;
    private final String desc;
    private final String descEn;

    public static String getVulLevelDesc(Integer code) {
        if (code == null) {
            return null;
        }
        for (VulLevelEnum value : values()) {
            if (value.getCode().equals(code)) {
                return value.getDesc();
            }
        }
        return null;
    }

    public static VulLevelEnum getVulLevelEnumByCode(Integer code) {
        if (code.equals(0) || code == null) {
            return null;
        }
        for (VulLevelEnum value : values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }
}
