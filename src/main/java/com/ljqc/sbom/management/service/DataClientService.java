package com.ljqc.sbom.management.service;

import com.ljqc.sbom.management.service.feignClient.dto.PlatformNameVersionReqVO;
import com.ljqc.sbom.management.service.feignClient.dto.PlatformNameVersionRespVO;
import com.ljqc.sbom.management.util.Result;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;

@FeignClient(value = "kb-api")
public interface DataClientService {


    @PostMapping(value = "/api/v1/vul/package/findListByPlatformAndNameAndVersion")
    Result<List<PlatformNameVersionRespVO>> findListByPlatformAndNameAndVersion(@RequestBody PlatformNameVersionReqVO reqVO);



}
