package com.ljqc.sbom.management.service.feignClient.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@Data
public class DataVulInfo {
    private String vulDescribeCn;
    private String vulDescribeEn;
    private String vulSolutionCn;
    private String vulSolutionEn;
    private String vulTitleCn;
    private String vulTitleEn;
    private String vulTypeCn;
    private String vulTypeEn;
    private String cnnvdId;
    private String cnnvdTitle;
    private String cvssV2;
    private BigDecimal cvss2BaseScore;
    private String cvssV3;
    private BigDecimal cvss3BaseScore;
    private String vulSeverityCn;
    private String vulPublished;
    private String ljVulId;
    private String cveId;
    private String expLevel;
    private String dataSource;
    //组件漏洞ID
    private String pkgVulId;
    private String url;
    //传CWE列表，带上中文名称
    private List<CweVO> cweVOList;
    //大棱镜给我传相关的漏洞修复链接，小棱镜的传空
    private List<String> patchUrls;
    //参考commit链接
    private List<String> commitUrls;
}
