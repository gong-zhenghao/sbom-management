package com.ljqc.sbom.management.service;

import com.alibaba.fastjson.JSONObject;
import com.ljqc.sbom.management.domain.SpdxPackage;
import com.ljqc.sbom.management.domain.SpdxProject;
import com.ljqc.sbom.management.dto.SpdxIngredientDTO;
import com.ljqc.sbom.management.dto.SpdxLicenseParam;
import com.ljqc.sbom.management.dto.SpdxPackageConditionDTO;
import com.ljqc.sbom.management.dto.SpdxPackageParam;
import com.ljqc.sbom.management.dto.SpdxPackageStatisticDTO;
import com.ljqc.sbom.management.dto.SpdxProjectInfoDTO;
import com.ljqc.sbom.management.dto.SpdxProjectLicenseInfoDTO;
import com.ljqc.sbom.management.dto.SpdxProjectSearchDTO;
import com.ljqc.sbom.management.dto.SpdxProjectVulInfoDTO;
import com.ljqc.sbom.management.dto.SpdxSbomDTO;
import com.ljqc.sbom.management.dto.SpdxVulListDTO;
import com.ljqc.sbom.management.vo.CommonResult;

import java.util.List;

public interface SpdxProjectService {

    Integer createSpdxProject(JSONObject param);

    boolean spdxProjectNameCheck(String name);

    CommonResult deleteSpdxProjectByIds(List<String> ids);

    JSONObject getSpdxList(SpdxProjectSearchDTO spdxProjectSearchDTO);

    JSONObject getProjectInfoCount();

    CommonResult<SpdxProjectInfoDTO> getProjectInfo(Integer spdxProjectId);

    SpdxProjectLicenseInfoDTO getSpdxProjectLicenseInfo(Integer spdxProjectId);

    CommonResult<SpdxProjectVulInfoDTO> getSpdxProjectVulInfo(Integer spdxProjectId);

    SpdxProject getSpdxProjectById(Integer spdxProjectId);

    void handleSpdxFileParse(JSONObject param);

    void updateStatus(SpdxProject spdxProject, String msg, Integer progress);

    SpdxPackage getPackageSolution(SpdxPackage spdxPackage);

    void updateStatusById(Integer id, String msg, Integer status);

    JSONObject getSpdxPackageList(SpdxPackageParam spdxPackageParam);

    SpdxPackageConditionDTO getPackageCondition(Integer spdxProjectId);

    SpdxPackageStatisticDTO getStatistic(Integer spdxProjectId);

    JSONObject getSpdxLicenseList(SpdxLicenseParam spdxLicenseParam);

    List<String> getLicenseAll(Integer spdxProjectId);

    JSONObject getPackageDetailList(Integer spdxProjectId, String name, int fileType, Integer pageNo, Integer pageSize);

    List<String> getVulPackages(Integer spdxProjectId);

    JSONObject getIngredientList(SpdxIngredientDTO spdxIngredientDTO);

    JSONObject getVulInfo(Integer spdxProjectId, Integer spdxPackageId, String field, String order, Integer ljVulScoreMax, Integer ljVulScoreMin, Integer pageNo, Integer pageSize);

    JSONObject getAllVul(SpdxVulListDTO spdxVulListDTO);

    SpdxSbomDTO getSbomInfo(Integer spdxProjectId);
}
