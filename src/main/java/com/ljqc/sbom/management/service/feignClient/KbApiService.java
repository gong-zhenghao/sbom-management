package com.ljqc.sbom.management.service.feignClient;


import com.ljqc.sbom.management.domain.Vulnerability;

import java.util.List;

public interface KbApiService {

    List<Vulnerability> getVulList(List<String> vulIdList);


}
