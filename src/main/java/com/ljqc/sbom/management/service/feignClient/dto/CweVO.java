package com.ljqc.sbom.management.service.feignClient.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CweVO implements Serializable {

    private String cweCode;

    private String cweName;
}
