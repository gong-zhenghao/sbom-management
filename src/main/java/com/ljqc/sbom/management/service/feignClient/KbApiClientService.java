package com.ljqc.sbom.management.service.feignClient;




import com.ljqc.sbom.management.service.feignClient.dto.DataVulInfo;
import com.ljqc.sbom.management.service.feignClient.dto.ListByLjVulIdsReqVO;
import com.ljqc.sbom.management.util.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "kb-api")
public interface KbApiClientService {
    /**
     * 批量获取漏洞信息
     */
    @PostMapping(value = "/api/v1/vul/listByLjVulIds")
    Result<List<DataVulInfo>> listByLjVulIds(@RequestBody ListByLjVulIdsReqVO reqVO);

}
