package com.ljqc.sbom.management.domain;

import com.ljqc.sbom.management.dto.LicenseInfoVo;
import com.ljqc.sbom.management.dto.MoveToPackage;
import com.ljqc.sbom.management.dto.VersionInfo;
import com.ljqc.sbom.management.dto.VersionVul;

import java.util.Date;
import java.util.List;

public class PackageInfo
{
    private Integer id;
    private String packageName;
    private String homePage;
    private List<VersionInfo> versionList;
    private List<VersionVul> versionVulList;
    private String platform;
    private String license;
    private LicenseInfoVo licenseInfoVo;
    private String descCn;
    private String descEn;
    private String latestVersion;
    private Date publishedTime;
    private String install;
    private Integer ljPackageId;
    private MoveToPackage moveToPackage;
    private String latestRelease;
    private Integer moveTo;


    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getPackageName()
    {
        return packageName;
    }

    public String getHomePage()
    {
        return homePage;
    }

    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
    }

    public void setPackageName(String packageName)
    {
        this.packageName = packageName;
    }

    public String getPlatform()
    {
        return platform;
    }

    public void setPlatform(String platform)
    {
        this.platform = platform;
    }

    public String getLicense()
    {
        return license;
    }

    public void setLicense(String license)
    {
        this.license = license;
    }

    public LicenseInfoVo getLicenseInfoVo()
    {
        return licenseInfoVo;
    }

    public void setLicenseInfoVo(LicenseInfoVo licenseInfoVo)
    {
        this.licenseInfoVo = licenseInfoVo;
    }

    public String getDescCn()
    {
        return descCn;
    }

    public void setDescCn(String descCn)
    {
        this.descCn = descCn;
    }

    public String getDescEn()
    {
        return descEn;
    }

    public void setDescEn(String descEn)
    {
        this.descEn = descEn;
    }

    public String getLatestVersion()
    {
        return latestVersion;
    }

    public void setLatestVersion(String latestVersion)
    {
        this.latestVersion = latestVersion;
    }

    public String getInstall()
    {
        return install;
    }

    public void setInstall(String install)
    {
        this.install = install;
    }

    public List<VersionInfo> getVersionList()
    {
        return versionList;
    }

    public List<VersionVul> getVersionVulList()
    {
        return versionVulList;
    }

    public void setVersionVulList(List<VersionVul> versionVulList)
    {
        this.versionVulList = versionVulList;
    }

    public void setVersionList(List<VersionInfo> versionList)
    {
        this.versionList = versionList;
    }

    public MoveToPackage getMoveToPackage()
    {
        return moveToPackage;
    }

    public void setMoveToPackage(MoveToPackage moveToPackage)
    {
        this.moveToPackage = moveToPackage;
    }

    public Date getPublishedTime()
    {
        return publishedTime;
    }

    public void setPublishedTime(Date publishedTime)
    {
        this.publishedTime = publishedTime;
    }

    public Integer getLjPackageId() {
        return ljPackageId;
    }

    public void setLjPackageId(Integer ljPackageId) {
        this.ljPackageId = ljPackageId;
    }

    public String getLatestRelease() {
        return latestRelease;
    }

    public void setLatestRelease(String latestRelease) {
        this.latestRelease = latestRelease;
    }

    public Integer getMoveTo() {
        return moveTo;
    }

    public void setMoveTo(Integer moveTo) {
        this.moveTo = moveTo;
    }
}
