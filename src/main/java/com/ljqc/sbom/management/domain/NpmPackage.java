package com.ljqc.sbom.management.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * npm包信息数据库实体
 */
@Entity
@Table(name = "npm_packages_info")
public class NpmPackage
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "myid")
    @GenericGenerator(name = "myid", strategy = "com.prism.parser.config.ManualInsertGenerator")
    private Integer id;
    private Integer ljPackageId;
    private String packageName;
    private String homePage;
    private String latestReleaseTime;
    private String latestRelease;
    private String description;
    private String license;
    private String verifiedLicense;
    private Integer moveTo;

    @Transient
    private Integer isInsert;

    public Integer getIsInsert()
    {
        return isInsert;
    }

    public void setIsInsert(Integer isInsert)
    {
        this.isInsert = isInsert;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getHomePage()
    {
        return homePage;
    }

    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
    }

    public String getLatestReleaseTime()
    {
        return latestReleaseTime;
    }

    public void setLatestReleaseTime(String latestReleaseTime)
    {
        this.latestReleaseTime = latestReleaseTime;
    }

    public String getLatestRelease()
    {
        return latestRelease;
    }

    public void setLatestRelease(String latestRelease)
    {
        this.latestRelease = latestRelease;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getLicense()
    {
        return license;
    }

    public void setLicense(String license)
    {
        this.license = license;
    }

    public String getVerifiedLicense()
    {
        return verifiedLicense;
    }

    public void setVerifiedLicense(String verifiedLicense)
    {
        this.verifiedLicense = verifiedLicense;
    }

    public Integer getLjPackageId()
    {
        return ljPackageId;
    }

    public void setLjPackageId(Integer ljPackageId)
    {
        this.ljPackageId = ljPackageId;
    }

    public Integer getMoveTo()
    {
        return moveTo;
    }

    public void setMoveTo(Integer moveTo)
    {
        this.moveTo = moveTo;
    }
}
