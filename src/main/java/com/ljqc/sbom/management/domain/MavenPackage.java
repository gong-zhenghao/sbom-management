package com.ljqc.sbom.management.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * maven包信息数据库实体
 */
@Entity
@Table(name = "maven_packages_info_new")
public class MavenPackage implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String packagePath;
    private String packageName;
    private String homePage;
    private String latestReleaseTime;
    private String latestRelease;
    private String description;
    private String license;
    private String verifiedLicense;
    private Integer ljPackageId;
    private Integer moveTo;
    private String tag;
    private String categories;

    @Transient
    private Integer isInsert;

    public Integer getIsInsert()
    {
        return isInsert;
    }

    public void setIsInsert(Integer isInsert)
    {
        this.isInsert = isInsert;
    }


    public String getCategories()
    {
        return categories;
    }

    public void setCategories(String categories)
    {
        this.categories = categories;
    }

    public String getTag()
    {
        return tag;
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }

    public Integer getLjPackageId()
    {
        return ljPackageId;
    }

    public void setLjPackageId(Integer ljPackageId)
    {
        this.ljPackageId = ljPackageId;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getPackagePath()
    {
        return packagePath;
    }

    public void setPackagePath(String packagePath)
    {
        this.packagePath = packagePath;
    }

    public String getPackageName()
    {
        return packageName;
    }

    public void setPackageName(String packageName)
    {
        this.packageName = packageName;
    }

    public String getHomePage()
    {
        return homePage;
    }

    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
    }

    public String getLatestReleaseTime()
    {
        return latestReleaseTime;
    }

    public void setLatestReleaseTime(String latestReleaseTime)
    {
        this.latestReleaseTime = latestReleaseTime;
    }

    public String getLatestRelease()
    {
        return latestRelease;
    }

    public void setLatestRelease(String latestRelease)
    {
        this.latestRelease = latestRelease;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getLicense()
    {
        return license;
    }

    public void setLicense(String license)
    {
        this.license = license;
    }

    public String getVerifiedLicense()
    {
        return verifiedLicense;
    }

    public void setVerifiedLicense(String verifiedLicense)
    {
        this.verifiedLicense = verifiedLicense;
    }

    public Integer getMoveTo()
    {
        return moveTo;
    }

    public void setMoveTo(Integer moveTo)
    {
        this.moveTo = moveTo;
    }
}
