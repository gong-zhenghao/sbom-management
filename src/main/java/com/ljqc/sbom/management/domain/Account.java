package com.ljqc.sbom.management.domain;


import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 账号数据库实体
 */
@Entity
@Table(name = "sys_account")
public class Account
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String account;
    private String psw;
    private String enterprise;
    private Integer status;
    private String email;
    @JSONField(name = "github_id")
    private Integer githubId;
    @JSONField(name = "gitee_id")
    private Integer giteeId;
    @JSONField(name = "gitee_local_id")
    private Integer giteeLocalId;
    @JSONField(name = "gitlab_id")
    private Integer gitlabId;
    @JSONField(name = "sso_id")
    private String ssoId;
    private String token;
    @JSONField(name = "enterprise_id")
    private Integer enterpriseId;
    private Integer role = 0;
    @JSONField(name = "github_token")
    private String githubToken;
    @JSONField(name = "gitee_token")
    private String giteeToken;
    @JSONField(name = "gitee_local_token")
    private String giteeLocalToken;
    @JSONField(name = "github_name")
    private String githubName;
    @JSONField(name = "gitee_name")
    private String giteeName;
    @JSONField(name = "gitlab_name")
    private String gitlabName;
    @JSONField(name = "last_login")
    private Long lastLogin;
    private Long deadline;
    private Long createTime;
    private Integer checkCount = 0;
    @JSONField(name = "vip_expire_time")
    private Date vipExpireTime;
    private String ldapDn;
    private String permission;
    private String phone;

    public Long getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Long createTime)
    {
        this.createTime = createTime;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getGiteeId()
    {
        return giteeId;
    }

    public void setGiteeId(Integer giteeId)
    {
        this.giteeId = giteeId;
    }

    public String getAccount()
    {

        return this.account;
    }

    public void setAccount(String account)
    {
        this.account = account;
    }

    public String getPsw()
    {
        return psw;
    }

    public void setPsw(String psw)
    {
        this.psw = psw;
    }

    public String getEnterprise()
    {
        return enterprise;
    }

    public void setEnterprise(String enterprise)
    {
        this.enterprise = enterprise;
    }

    public Integer getStatus()
    {
        return status;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Integer getGithubId()
    {
        return githubId;
    }

    public void setGithubId(Integer githubId)
    {
        this.githubId = githubId;
    }

    public Integer getGitlabId()
    {
        return gitlabId;
    }

    public void setGitlabId(Integer gitlabId)
    {
        this.gitlabId = gitlabId;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public Integer getEnterpriseId()
    {
        return enterpriseId;
    }

    public void setEnterpriseId(Integer enterpriseId)
    {
        this.enterpriseId = enterpriseId;
    }

    public Integer getRole()
    {
        return role;
    }

    public void setRole(Integer role)
    {
        this.role = role;
    }

    public String getGithubToken()
    {
        return githubToken;
    }

    public void setGithubToken(String githubToken)
    {
        this.githubToken = githubToken;
    }

    public String getGiteeToken()
    {
        return giteeToken;
    }

    public void setGiteeToken(String giteeToken)
    {
        this.giteeToken = giteeToken;
    }

    public String getGithubName()
    {
        return githubName;
    }

    public void setGithubName(String githubName)
    {
        this.githubName = githubName;
    }

    public String getGiteeName()
    {
        return giteeName;
    }

    public void setGiteeName(String giteeName)
    {
        this.giteeName = giteeName;
    }

    public Long getLastLogin()
    {
        return lastLogin;
    }

    public void setLastLogin(Long lastLogin)
    {
        this.lastLogin = lastLogin;
    }

    public Long getDeadline()
    {
        return deadline;
    }

    public void setDeadline(Long deadline)
    {
        this.deadline = deadline;
    }

    public Integer getGiteeLocalId()
    {
        return giteeLocalId;
    }

    public void setGiteeLocalId(Integer giteeLocalId)
    {
        this.giteeLocalId = giteeLocalId;
    }

    public String getGiteeLocalToken()
    {
        return giteeLocalToken;
    }

    public void setGiteeLocalToken(String giteeLocalToken)
    {
        this.giteeLocalToken = giteeLocalToken;
    }

    public String getGitlabName()
    {
        return gitlabName;
    }

    public void setGitlabName(String gitlabName)
    {
        this.gitlabName = gitlabName;
    }

    public Integer getCheckCount()
    {
        return checkCount == null ? 0 : checkCount;
    }

    public void setCheckCount(Integer checkCount)
    {
        this.checkCount = checkCount == null ? 0 : checkCount;
    }

    public Date getVipExpireTime()
    {
        return vipExpireTime;
    }

    public void setVipExpireTime(Date vipExpireTime)
    {
        this.vipExpireTime = vipExpireTime;
    }

    public String getLdapDn()
    {
        return ldapDn;
    }

    public void setLdapDn(String ldapDn)
    {
        this.ldapDn = ldapDn;
    }

    public String getPermission()
    {
        return permission;
    }

    public void setPermission(String permission)
    {
        this.permission = permission;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getSsoId()
    {
        return ssoId;
    }

    public void setSsoId(String ssoId)
    {
        this.ssoId = ssoId;
    }
}

