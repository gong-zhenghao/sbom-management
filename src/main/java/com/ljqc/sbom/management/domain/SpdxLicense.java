package com.ljqc.sbom.management.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spdx_license")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpdxLicense {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer spdxProjectId;
    private Integer spdxPackageId;
    private String license;
    private Integer riskLevel;
    private String category;

}
