package com.ljqc.sbom.management.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spdx_package")
@Getter
@Setter
public class SpdxPackage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer spdxProjectId;
    private Integer ljPackageId;
    private String spdxId;
    private String repoName;
    private String version;
    private String platform;
    private String license;
    private Integer licenseRelation;
    private String comment;
    private String packageFileName;
    private Boolean inhouse = false;
    private Integer moveTo;
    private String moveToSolution;
    private String solution;
    private String cleanVersion;
    private String homePage;
    private String summary;
    private String description;
    private String downloadLocation;
    private String supplier;
    private String originator;
    private String sourceInfo;
    private String licenseConcluded;
    private String licenseDeclared;
    private String licenseComments;
    private String copyrightText;
    private String verificationCode;
    private String checksum;
    private String relationship;


}
