package com.ljqc.sbom.management.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpdxMQDto {

    /**
     * spdx项目id
     */
    private Integer spdxProjectId;

    /**
     * 检测文件路径
     */
    private String dirPath;
}
