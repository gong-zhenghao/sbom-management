package com.ljqc.sbom.management.dto;

import com.ljqc.sbom.management.service.feignClient.dto.CweVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Builder
@Data
public class SpdxVulInfoDTO {
    @ApiModelProperty("漏洞编号")
    private String vulId;
    @ApiModelProperty("cve编号")
    private String cveId;
    @ApiModelProperty("cwes")
    private String cwes;
    @ApiModelProperty("危害等级(0:未评级 1:低危 2:中危 3:高危 4:严重)")
    private Integer vulLevel;
    @ApiModelProperty("评分")
    private Float cvssScore;
    @ApiModelProperty("漏洞名称")
    private String vulTitle;
    @ApiModelProperty("漏洞发布时间")
    private String vulPublished;
    @ApiModelProperty("利用成熟度")
    private Integer exposureLevel;
    @ApiModelProperty("是否有修复方案")
    private Integer solveFlag;
    @ApiModelProperty("漏洞优先级评分")
    private BigDecimal ljVulScore;
    @ApiModelProperty("cwes")
    private List<CweVO> cweList;
}
