package com.ljqc.sbom.management.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class VulPackageInfoVO {

    @Schema(name = "主键ID")
    private Long id;

    @Schema(name = "创建时间")
    private LocalDateTime createTime;

    @Schema(name = "修改时间")
    private LocalDateTime modifyTime;

    @Schema(name = "状态")
    private String status;

    @Schema(name = "重复ID")
    private String duplicateId;

    @Schema(name = "棱镜漏洞标识")
    private String vulId;

    @Schema(name = "cve_id")
    private String cveId;

    @Schema(name = "cnvd_id")
    private String cnvdId;

    @Schema(name = "cnnvd_id")
    private String cnnvdId;

    @Schema(name = "是否开源")
    private Boolean isOpenSource;

    @Schema(name = "审核状态")
    private Integer verifyStatus;

    @Schema(name = "首次审核时间")
    private LocalDateTime firstVerifyTime;

    @Schema(name = "审核时间")
    private LocalDateTime verifyTime;

    @Schema(name = "中文标题")
    private String titleCn;

    @Schema(name = "英文标题")
    private String titleEn;

    @Schema(name = "中文描述")
    private String descriptionCn;

    @Schema(name = "英文描述")
    private String descriptionEn;

    @Schema(name = "提交时间")
    private LocalDateTime submitTime;

    @Schema(name = "公开时间")
    private LocalDateTime publishedTime;

    @Schema(name = "修改时间")
    private LocalDateTime modifiedTime;

    @Schema(name = "cvss2向量字符串")
    private String cvss2Vector;

    @Schema(name = "cvss2分数")
    private BigDecimal cvss2BaseScore;

    @Schema(name = "cvss3向量字符串")
    private String cvss3Vector;

    @Schema(name = "cvss3分数")
    private BigDecimal cvss3BaseScore;

    @Schema(name = "危险等级")
    private Integer severity;

    @Schema(name = "中文修复方案")
    private String solutionCn;

    @Schema(name = "英文修复方案")
    private String solutionEn;

    @Schema(name = "中文缓解措施")
    private String relieveCn;

    @Schema(name = "英文缓解措施")
    private String relieveEn;

    @Schema(name = "最后修改人")
    private String modifiedUserName;

    @Schema(name = "首次来源")
    private String sourceType;

    @Schema(name = "批次")
    private String flag;

    @Schema(name = "漏洞类型")
    private String cwes;

    @Schema(name = "利用级别")
    private Integer level;

    @Schema(name = "匹配原始串")
    private String match;

    @Schema(name = "受影响版本表达式(棱镜)")
    private String affectedVersionExpression;

    @Schema(name = "安全版本表达式(棱镜)")
    private String unaffectedVersionExpression;

    @Schema(name = "棱镜组件漏洞标识")
    private String pkgVulId;

    @Schema(name = "受影响组件")
    private Long affectPackageId;

    @Schema(name = "受影响组件名")
    private String affectPackageName;

    @Schema(name = "受影响类型")
    private String affectPackageType;
    private String url;
}
