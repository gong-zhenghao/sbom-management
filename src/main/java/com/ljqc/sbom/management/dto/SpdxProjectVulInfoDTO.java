package com.ljqc.sbom.management.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *  spdx项目概览 漏洞风险
 */
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpdxProjectVulInfoDTO {
    @ApiModelProperty("超危")
    private Integer vulCritical = 0;
    @ApiModelProperty("高危")
    private Integer vulHigh = 0;
    @ApiModelProperty("低危")
    private Integer vulLow = 0;
    @ApiModelProperty("中危")
    private Integer vulMid = 0;
    @ApiModelProperty("未评级")
    private Integer vulUnknown= 0;

    @ApiModelProperty("安全漏洞总数")
    private Integer vulCount= 0;
    @ApiModelProperty("组件数")
    private Integer packageCount= 0;
}
