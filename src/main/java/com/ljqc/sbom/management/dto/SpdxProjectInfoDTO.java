package com.ljqc.sbom.management.dto;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SpdxProjectInfoDTO {
    private Integer id;
    private String name;
    private Integer type;
    private String projectVersion;
    private String projectLicense;
    private String licenseListVersion;
    private String spdxId;
    private String creatorPerson;
    private String creatorOrganization;
    private String creatorTool;
    private String documentNamespace;
    private String documentComment;
    private String creatorComment;
    private String created;
    private Integer netType;
    private Integer status = 0;
    private String msg;
    private Integer progress;
    private Integer repoVulLevel;
    private Integer licenseRiskLevel;
    private Long createTime;
    private Long updateTime;
    private String account;
}
