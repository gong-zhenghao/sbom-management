package com.ljqc.sbom.management.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
public class MoveToPackage implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer ljPackageId;
    private String packagePath;
    @Transient
    private MoveToPackage moveToPackage;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getLjPackageId()
    {
        return ljPackageId;
    }

    public void setLjPackageId(Integer ljPackageId)
    {
        this.ljPackageId = ljPackageId;
    }

    public String getPackagePath()
    {
        return packagePath;
    }

    public void setPackagePath(String packagePath)
    {
        this.packagePath = packagePath;
    }

    public MoveToPackage getMoveToPackage()
    {
        return moveToPackage;
    }

    public void setMoveToPackage(MoveToPackage moveToPackage)
    {
        this.moveToPackage = moveToPackage;
    }
}
