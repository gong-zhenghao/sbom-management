package com.ljqc.sbom.management.dto;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class VersionInfo implements Serializable
{
    private String version;
    private String install;
    private String publishedTime;
    private String license;
    private LicenseInfoVo licenseInfoVo;
    private Integer affectProjectNumber;
    private boolean used;
    private List<String> vulList = new LinkedList<>();
    private int[] vulLevelCount=new int[5];

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getInstall()
    {
        return install;
    }

    public void setInstall(String install)
    {
        this.install = install;
    }

    public String getPublishedTime()
    {
        return publishedTime;
    }

    public void setPublishedTime(String publishedTime)
    {
        this.publishedTime = publishedTime;
    }

    public String getLicense()
    {
        return license;
    }

    public void setLicense(String license)
    {
        this.license = license;
    }

    public LicenseInfoVo getLicenseInfoVo()
    {
        return licenseInfoVo;
    }

    public void setLicenseInfoVo(LicenseInfoVo licenseInfoVo)
    {
        this.licenseInfoVo = licenseInfoVo;
    }

    public List<String> getVulList()
    {
        return vulList;
    }

    public void addVul(String vul)
    {
        this.vulList.add(vul);
    }

    public void setVulList(List<String> vulList)
    {
        this.vulList = vulList;
    }

    public int[] getVulLevelCount()
    {
        return vulLevelCount;
    }

    public void setVulLevelCount(int[] vulLevelCount)
    {
        this.vulLevelCount = vulLevelCount;
    }

    public Integer getAffectProjectNumber()
    {
        return affectProjectNumber;
    }

    public void setAffectProjectNumber(Integer affectProjectNumber)
    {
        this.affectProjectNumber = affectProjectNumber;
    }

    public boolean isUsed()
    {
        return affectProjectNumber>0;
    }

    public void setUsed(boolean used)
    {
        this.used = used;
    }
}
