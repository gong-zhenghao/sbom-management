package com.ljqc.sbom.management.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class SpdxPackageParam {
    @NotNull(message = "项目id不能为空")
    private Integer projectId;

    @NotNull(message = "当前页不能为空")
    private Integer pageNo;

    @NotNull(message = "分页条数不能为空")
    private Integer pageSize;

    private String repoName;

    private String platform;

    private Integer inhouse;

    /**
     * 排序字段
     */
    //@NotNull(message = "排序字段集合不能为空")该字段前端必传,为复用此处代码取消此处非空校验不影响原逻辑
    private List<String> orderNameList;

    /**
     * 排序规则 DESC：降序 ASC：升序
     */
    private String orderRule;



}
