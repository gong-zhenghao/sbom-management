package com.ljqc.sbom.management.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SpdxProjectSearchDTO {

    private List<Integer> vulLevel;    // 安全风险等级
    private String search;  // 项目名称
    private Integer page;
    private Integer pageSize;

}
