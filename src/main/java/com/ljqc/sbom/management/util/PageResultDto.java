package com.ljqc.sbom.management.util;

import lombok.Data;

import java.util.List;

@Data
public class PageResultDto {
    /**
     *  数据总页数
    */
    private long totalPages;
    /**
     *  数据总条数
    */
    private long totalRecords;
    /**
     *  每页数据量
    */
    private int pageSize;
    /**
     *  当前页数
    */
    private long currentPage;
    /**
     *  数据
    */
    private List records;

}
