package com.ljqc.sbom.management.util;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class PageResultUtils {
    /**
     *  获取分页数据
    */
    public static PageResultDto getPageResult(Integer page, Integer limit, List list) {
        PageResultDto pageResult = new PageResultDto();
        try {
            //总数量
            int total = list.size();
            //第几行数据开始
            int fromIndex = (Integer.valueOf(page)-1)*Integer.valueOf(limit);
            //第几行结束
            int toIndex = fromIndex + Integer.valueOf(limit);
            if (toIndex > total){
                toIndex = total;
            }
            if (fromIndex <= total){
                List pageList = list.subList(fromIndex, toIndex);
                pageResult.setTotalRecords(total);
                pageResult.setCurrentPage(page);
                pageResult.setPageSize(limit);
                pageResult.setTotalPages((total - 1)/Integer.valueOf(limit) + 1);
                pageResult.setRecords(pageList);
            }
        }catch (Exception e){
            log.error("手动分页失败"+e.getMessage());
        }
        return pageResult;
    }
}
