package com.ljqc.sbom.management.util;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.time.Instant;

@ToString(callSuper = true)
@EqualsAndHashCode
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ApiModel("通用返回值包装类")
public class Result<T> implements Serializable {
    private static final int SUCCESS_CODE = ResultCode.SUCCESS.getCode();
    private static final int DEFAULT_ERR_CODE = ResultCode.UNKNOWN_ERROR.getCode();
    private static final String DEFAULT_ERR_MSG = ResultCode.UNKNOWN_ERROR.getMessage();

    /**
     * ===========================
     */
    @ApiModelProperty(name = "code", value = "业务状态码 200表示正常")
    private int code = SUCCESS_CODE;
    @ApiModelProperty("异常提示信息,出现业务异常时非空")
    private String message = null;
    @ApiModelProperty("业务数据")
    private T data = null;
    @ApiModelProperty(hidden = true)
    private final long timestamp = Instant.now().toEpochMilli();
    private boolean success;

    //****************静态方法们********************
    public static <T> Result<T> ok() {
        return new Result<>();
    }

    /**
     * 成功并返回自定义提示信息
     */
    public static <T> Result<T> ok(T data,String msg) {
        Result<T> result = new Result<>();
        result.setErrMsg(msg);
        result.setData(data);
        return result;
    }

    public static <T> Result<T> ok(T data) {
        Result<T> result = ok();
        result.setData(data);
        return result;
    }

    public static <T> Result<T> fail() {
        Result<T> result = new Result<>();
        result.setErrCode(DEFAULT_ERR_CODE);
        result.setErrMsg(DEFAULT_ERR_MSG);
        return result;
    }

    public static <T> Result<T> fail(int errCode, String errMsg) {
        Result<T> result = fail();
        result.setErrCode(errCode);
        result.setErrMsg(errMsg);
        return result;
    }

    public static <T> Result<T> fail(String errMsg) {
        Result<T> result = fail();
        result.setErrCode(DEFAULT_ERR_CODE);
        result.setErrMsg(errMsg);
        return result;
    }

    public static <T> Result<T> fail(ResultCode errCode) {
        return fail(errCode.getCode(), errCode.getMessage());
    }

    public static <T> Result<T> fail(ResultCode errCode, String customMsg) {
        return fail(errCode.getCode(), customMsg);
    }

    /***************实例方法******************/

    /**
     * 判断是否成功
     *
     * @return true or false
     */
    public boolean isSuccess() {
        return getCode() == SUCCESS_CODE;
    }

    /**
     * 设置负载数据
     *
     * @param data 返回数据
     * @return this本身
     */
    public Result<T> setData(T data) {
        this.data = data;
        return this;
    }

    /**
     * 设置错误码
     *
     * @param errCode 错误码
     */
    public void setErrCode(int errCode) {
        if (errCode == SUCCESS_CODE) {
            throw new RuntimeException("Your code is " + errCode + ".Which only means SUCCESS");
        }
        this.code = errCode;
    }

    // ==============快捷方式===================

    /**
     * 设置错误消息
     *
     * @param errMsg 错误消息
     */
    public void setErrMsg(String errMsg) {
        this.message = errMsg;
    }
}
