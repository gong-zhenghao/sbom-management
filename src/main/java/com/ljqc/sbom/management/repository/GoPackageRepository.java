package com.ljqc.sbom.management.repository;

import com.ljqc.sbom.management.domain.GoPackage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoPackageRepository extends JpaRepository<GoPackage, Integer>
{

    GoPackage findByLjPackageId(Integer packageId);

}
