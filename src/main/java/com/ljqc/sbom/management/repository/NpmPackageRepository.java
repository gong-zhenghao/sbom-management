package com.ljqc.sbom.management.repository;

import com.ljqc.sbom.management.domain.NpmPackage;
import org.springframework.data.jpa.repository.JpaRepository;


public interface NpmPackageRepository extends JpaRepository<NpmPackage, Integer> {

    NpmPackage findByLjPackageId(Integer packageId);

}
