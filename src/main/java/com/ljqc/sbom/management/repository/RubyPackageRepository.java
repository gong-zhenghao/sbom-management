package com.ljqc.sbom.management.repository;

import com.ljqc.sbom.management.domain.RubyPackage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RubyPackageRepository extends JpaRepository<RubyPackage, Integer>
{
    RubyPackage findByLjPackageId(Integer ljPackageId);

}
