package com.ljqc.sbom.management.repository;

import com.ljqc.sbom.management.domain.ConanPackage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ConanPackageRepository extends JpaRepository<ConanPackage, Integer>
{
    ConanPackage findByLjPackageId(Integer ljPackageId);

}
