package com.ljqc.sbom.management.repository;

import com.ljqc.sbom.management.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    @Query("SELECT id FROM Account WHERE enterpriseId = ?1")
    List<Integer> findAccountIdsByEnterpriseId(Integer enterpriseId);

    @Query(nativeQuery = true, value = "SELECT role,enterprise_id FROM sys_account WHERE id = ?1")
    Map<String, Object> findRoleAndEnterpriseIdById(Integer userId);
}
