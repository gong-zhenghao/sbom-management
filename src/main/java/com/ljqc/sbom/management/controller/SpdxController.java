package com.ljqc.sbom.management.controller;

import com.alibaba.fastjson.JSONObject;
import com.ljqc.sbom.management.domain.SpdxProject;
import com.ljqc.sbom.management.dto.SpdxIngredientDTO;
import com.ljqc.sbom.management.dto.SpdxLicenseParam;
import com.ljqc.sbom.management.dto.SpdxPackageConditionDTO;
import com.ljqc.sbom.management.dto.SpdxPackageParam;
import com.ljqc.sbom.management.dto.SpdxPackageStatisticDTO;
import com.ljqc.sbom.management.dto.SpdxProjectInfoDTO;
import com.ljqc.sbom.management.dto.SpdxProjectLicenseInfoDTO;
import com.ljqc.sbom.management.dto.SpdxProjectSearchDTO;
import com.ljqc.sbom.management.dto.SpdxProjectVulInfoDTO;
import com.ljqc.sbom.management.dto.SpdxSbomDTO;
import com.ljqc.sbom.management.dto.SpdxVulListDTO;
import com.ljqc.sbom.management.service.SpdxProjectService;
import com.ljqc.sbom.management.util.Result;
import com.ljqc.sbom.management.vo.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@Api("spdx相关")
@RestController
@Slf4j
@RequestMapping("/spdx")
public class SpdxController {

    @Autowired
    SpdxProjectService spdxProjectService;

    @ApiOperation(value = "创建spdx项目")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CommonResult createSpdxProject(@RequestBody JSONObject param) {
        return new CommonResult(200, spdxProjectService.createSpdxProject(param), "创建项目成功");
    }

    @ApiOperation(value = "spdx项目名称校验")
    @PostMapping("/projectNameCheck")
    public CommonResult projectNameCheck(@RequestBody JSONObject param) {
        String name = param.getString("name");
        boolean exist = spdxProjectService.spdxProjectNameCheck(name);
        if (exist) {
            return new CommonResult(500, "项目名称重复");
        }
        return new CommonResult(200, null, "");
    }

    @ApiOperation(value = "批量删除spdx项目信息")
    @PostMapping(value = "/delete", consumes = "application/json")
    public CommonResult deleteProjectByIds(@RequestBody JSONObject param) {
        try {
            String ids = param.getString("ids");
            return spdxProjectService.deleteSpdxProjectByIds(Arrays.asList(ids.split(",")));
        } catch (Exception e) {
            log.error("批量删除项目异常:", e);
            return new CommonResult(500, "删除失败");
        }
    }

    @ApiOperation(value = "spdx项目列表")
    @PostMapping(value = "/list")
    public JSONObject getProjectInfoList(@RequestBody SpdxProjectSearchDTO spdxProjectSearchDTO) {
        return spdxProjectService.getSpdxList(spdxProjectSearchDTO);
    }

    @ApiOperation(value = "spdx安全等级风险统计")
    @GetMapping(value = "/filter/count")
    public JSONObject getProjectInfoCount() {
        return spdxProjectService.getProjectInfoCount();
    }

    @ApiOperation(value = "spdx项目概览")
    @GetMapping(value = "/info")
    public CommonResult<SpdxProjectInfoDTO> getSpdxProjectInfo(@RequestParam("spdxProjectId") Integer spdxProjectId) {
        return spdxProjectService.getProjectInfo(spdxProjectId);
    }


    @ApiOperation(value = "spdx许可证概览")
    @GetMapping(value = "/getProjectLicenseInfo")
    public CommonResult<SpdxProjectLicenseInfoDTO> getProjectLicenseInfo(@RequestParam("spdxProjectId") Integer spdxProjectId) {
        return new CommonResult<>(200, spdxProjectService.getSpdxProjectLicenseInfo(spdxProjectId), "");
    }

    @ApiOperation(value = "spdx漏洞概览")
    @GetMapping(value = "/getProjectVulInfo")
    public CommonResult<SpdxProjectVulInfoDTO> getProjectVulInfo(@RequestParam("spdxProjectId") Integer spdxProjectId) {
        return spdxProjectService.getSpdxProjectVulInfo(spdxProjectId);
    }

    @ApiOperation(value = "查看解析进度")
    @RequestMapping(value = "/progress", method = RequestMethod.GET)
    public SpdxProject getTaskProgress(Integer spdxProjectId) {
        return spdxProjectService.getSpdxProjectById(spdxProjectId);
    }


    @ApiOperation(value = "spdx文件内容解析")
    @RequestMapping(value = "/parse", method = RequestMethod.POST)
    public void parseSpdxFile(@RequestBody JSONObject param) {
        spdxProjectService.handleSpdxFileParse(param);
    }

    @ApiOperation(value = "spdx组件列表")
    @RequestMapping(value = "/getSpdxPackageList", method = RequestMethod.POST)
    public Result<JSONObject> getSpdxPackageList(@RequestBody SpdxPackageParam spdxPackageParam) {
        return Result.ok(spdxProjectService.getSpdxPackageList(spdxPackageParam));
    }

    @ApiOperation(value = "spdx组件筛选")
    @RequestMapping(value = "/package/getCondition", method = RequestMethod.GET)
    public Result<SpdxPackageConditionDTO> getPackageCondition(Integer spdxProjectId) {
        return Result.ok(spdxProjectService.getPackageCondition(spdxProjectId));
    }

    @ApiOperation(value = "spdx组件风险统计")
    @RequestMapping(value = "/package/getStatistic", method = RequestMethod.GET)
    public Result<SpdxPackageStatisticDTO> getStatistic(Integer spdxProjectId) {
        return Result.ok(spdxProjectService.getStatistic(spdxProjectId));
    }

    @ApiOperation(value = "spdx许可证列表")
    @RequestMapping(value = "/license/getLicenseList", method = RequestMethod.POST)
    public Result<JSONObject> getLicenseList(@RequestBody SpdxLicenseParam spdxPackageParam) {
        return Result.ok(spdxProjectService.getSpdxLicenseList(spdxPackageParam));
    }

    @ApiOperation(value = "spdx许可证筛选")
    @RequestMapping(value = "/license/getCondition", method = RequestMethod.GET)
    public Result<List<String>> getLicenseCondition(Integer spdxProjectId) {
        return Result.ok(spdxProjectService.getLicenseAll(spdxProjectId));
    }
    @ApiOperation(value = "SBOM相关数据信息")
    @RequestMapping(value = "/sbomInfo", method = RequestMethod.GET)
    public SpdxSbomDTO getSbomInfo(Integer spdxProjectId) {
       return spdxProjectService.getSbomInfo(spdxProjectId);
    }

    @ApiOperation(value = "spdx引入方式列表")
    @RequestMapping(value = "/getPackageDetailList", method = RequestMethod.GET)
    public Result<JSONObject> getPackageDetailList(Integer spdxProjectId, String name, int fileType, Integer pageNo, Integer pageSize) {
        return Result.ok(spdxProjectService.getPackageDetailList(spdxProjectId, name, fileType, pageNo, pageSize));
    }

    @ApiOperation(value = "spdx漏洞列表包管理器")
    @RequestMapping(value = "/vul/getVulPackages", method = RequestMethod.GET)
    public Result<List<String>> getVulPackages(Integer spdxProjectId) {
        return Result.ok(spdxProjectService.getVulPackages(spdxProjectId));
    }

    @ApiOperation(value = "spdx组件漏洞列表")
    @RequestMapping(value = "/vul/getIngredientList", method = RequestMethod.POST)
    public Result<JSONObject> getIngredientList(@RequestBody SpdxIngredientDTO spdxIngredientDTO) {
        return Result.ok(spdxProjectService.getIngredientList(spdxIngredientDTO));
    }

    @GetMapping(value = "/vul/getVulInfo")
    public Result<JSONObject> getVulInfo(Integer spdxProjectId, Integer spdxPackageId, String field, String order, Integer ljVulScoreMax, Integer ljVulScoreMin,
                                         @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                         @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize) {
        return Result.ok(spdxProjectService.getVulInfo(spdxProjectId, spdxPackageId, field, order, ljVulScoreMax, ljVulScoreMin, pageNo, pageSize));
    }

    @ApiOperation(value = "spdx漏洞列表")
    @RequestMapping(value = "/vul/getAllVul", method = RequestMethod.POST)
    public Result<JSONObject> getAllVul(@RequestBody SpdxVulListDTO spdxVulListDTO) {
        return Result.ok(spdxProjectService.getAllVul(spdxVulListDTO));
    }

}