package com.ljqc.sbom.management.vo;

import java.io.Serializable;

public class CommonResult<T> implements Serializable
{
    private String msg;
    private Integer code;
    private T data;

    public CommonResult()
    {
    }

    public CommonResult(Integer code, T data)
    {
        this.code = code;
        this.data = data;
    }

    public CommonResult(Integer code, T data, String msg)
    {
        this.msg = msg;
        this.code = code;
        this.data = data;
    }

    public CommonResult(Integer code, String msg)
    {
        this.msg = msg;
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public Integer getCode()
    {
        return code;
    }

    public void setCode(Integer code)
    {
        this.code = code;
    }

    public T getData()
    {
        return data;
    }

    public void setData(T data)
    {
        this.data = data;
    }
}
